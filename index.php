
<!DOCTYPE html>
<html lang="zxx" dir="ltr">
	
<!-- Mirrored from riettsruff.com/projects/web-templates/metronal/preview/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 13 Apr 2019 11:37:56 GMT -->
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

		<!-- Description -->
		<meta name="description" content="Metronal is a clean and elegant Personal Portfolio Page. Metronal comes with a very attractive, simple and beautiful design with the effectiveness of colorful simplicity.">

		<!-- Keywords -->
		<meta name="keywords" content="Metronal, Personal Page, Portfolio, CV, Resume, One Page, Web Template">

		<!-- Title -->
		<title><?= "My Personal Portfolio" ?></title>

		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,800%7COpen+Sans:400,400i,700" rel="stylesheet">

		<!-- CSS Files -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/fontawesome/css/all.min.css" rel="stylesheet">
		<link href="css/magnific-popup.css" rel="stylesheet">
		<link href="css/skins/skin1.css" rel="stylesheet">
		<link href="css/custom.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>
		<!-- Pre Load Starts -->
	    <div id="pre-load">
	    	<div class="load-circle"></div>
	    </div>
	    <!-- Pre Load Ends -->
		<!-- Wrapper Starts -->
		<div id="wrapper">
			<div class="container-fluid">
				<div class="row no-gutters v-full">
					<!-- Left Wrapper Starts -->
					<div id="left-wrapper" class="col-12 col-lg-8">
						<!-- Main Content (Home) Starts -->
						<div id="home" class="main-content">
							<!-- Content Hanging On Home Section Starts -->
							<div class="hanging">
								<div class="logo-hanging">
									<i class="fas fa-globe"></i>
								</div>
								<div class="text-hanging">
									<div class="word">
										<h6><?= "HELLO" ?><span><?= "WORLD" ?></span></h6>
									</div>
								</div>
							</div>
							<!-- Content Hanging On Home Section Ends -->
							<!-- Inner Content Starts -->
							<div class="inner-content">
								<!-- Head Content Starts -->
								<div class="head-content">
									<h3><?= '<span class="name">MUKAMAT RENDI S (1614321046)</span>' ?></h3>
								</div>
								<!-- Head Content Ends -->
								<!-- Content Starts -->
								<div class="content">
									<!-- About Menu Starts -->
									<div id="about-menu" class="box-wrapper">
										<div class="inner-box-wrapper">
											<!-- <div class="icon-wrapper">
												<i class="fas fa-id-card fa-2x"></i>
											</div> -->
											<div class="box">
												<div class="valign-center">
													<i class="fas fa-id-card fa-3x"></i>
													<h5><?= "About" ?> <span><?= "Me" ?></span></h5>
												</div>
											</div>
										</div>
									</div>
									<!-- About Menu Ends -->
								</div>
								<!-- Content Ends -->
							</div>
							<!-- Inner Content Ends -->
						</div>
						<!-- Main Content (Home) Ends -->
						<!-- Main Content (About) Starts -->
						<div id="about" class="main-content">
							<!-- Close Button Starts -->
							<i class="close-button fas fa-times-circle fa-2x"></i>
							<!-- Close Button Ends -->
							<!-- Content Hanging On About Section Starts -->
							<div class="hanging">
								<div class="logo-hanging">
									<i class="fas fa-id-card"></i>
								</div>
								<div class="text-hanging">
									<div class="word">
										<h6><?= "ABOUT" ?><span><?= "ME" ?></span></h6>
									</div>
								</div>
							</div>
							<!-- Content Hanging On About Section Ends -->
							<!-- Inner Content Starts -->
							<div class="inner-content">
								<!-- Head Content Starts -->
								<div class="head-content">
									<h3>About <span>Me</span></h3>
								</div>
								<!-- Head Content Ends -->
								<!-- Content Starts -->
								<div class="content">
									<!-- Personal Info Starts -->
									<div id="personal-info">
										<!-- Personal Info Heading Starts -->
										<h5><?= "Personal Info" ?></h5>
										<!-- Personal Info Heading Ends -->
										<div class="row no-gutters">
											<!-- Profile Picture Starts -->
											<div class="profile-picture col-md-2 col-sm-3 col-12"></div>
											<!-- Profile Picture Ends -->
											<!-- Single Profile Starts -->
											<div class="profile col-12 col-sm-6">
												<ul>
													<!-- Single Content On Profile Starts -->
													<li>
														<span class="label">
															<i class="fas fa-angle-double-right"></i><span><?= "Full Name" ?></span>
														</span>
														<span class="dash">-</span>
														<span class="value"><?= "Mukamat Rendi Siswanto" ?></span>
													</li>
													<!-- Single Content On Profile Ends -->
													<!-- Single Content On Profile Starts -->
													<li>
														<span class="label">
															<i class="fas fa-angle-double-right"></i><span><?= "Nick Name" ?></span>
														</span>
														<span class="dash">-</span>
														<span class="value"><?= "Rendi" ?></span>
													</li>
													<!-- Single Content On Profile Ends -->
													<!-- Single Content On Profile Starts -->
													<li>
														<span class="label">
															<i class="fas fa-angle-double-right"></i><span><?= "Date of Birth" ?></span>
														</span>
														<span class="dash">-</span>
														<span class="value"><?= "11 July 1997" ?></span>
													</li>
													<!-- Single Content On Profile Ends -->
													<!-- Single Content On Profile Starts -->
													<li>
														<span class="label">
															<i class="fas fa-angle-double-right"></i><span><?= "Faculty" ?></span>
														</span>
														<span class="dash">-</span>
														<span class="value"><?= "Technical Information" ?></span>
													</li>
													<!-- Single Content On Profile Ends -->
												</ul>
											</div>
											<!-- Single Profile Ends -->
											<!-- Single Profile Starts -->
											<div class="profile col-12 col-sm-6">
												<ul>
													<!-- Single Content On Profile Starts -->
													<li>
														<span class="label">
															<i class="fas fa-angle-double-right"></i><span><?= "Generation" ?></span>
														</span>
														<span class="dash">-</span>
														<span class="value"><?= "2016" ?></span>
													</li>
													<!-- Single Content On Profile Ends -->
													<!-- Single Content On Profile Starts -->
													<li>
														<span class="label">
															<i class="fas fa-angle-double-right"></i><span><?= "Email" ?></span>
														</span>
														<span class="dash">-</span>
														<span class="value"><?= "rendyredz@gmail.com" ?></span>
													</li>
													<!-- Single Content On Profile Ends -->
													<!-- Single Content On Profile Starts -->
													<li>
														<span class="label">
															<i class="fas fa-angle-double-right"></i><span><?= "Address" ?></span>
														</span>
														<span class="dash">-</span>
														<span class="value"><?= "Kediri" ?></span>
													</li>
													<!-- Single Content On Profile Ends -->
													<!-- Single Content On Profile Starts -->
													<li>
														<span class="label">
															<i class="fas fa-angle-double-right"></i><span><?= "Languages" ?></span>
														</span>
														<span class="dash">-</span>
														<span class="value"><?= "Indonesia & English" ?></span>
													</li>
													<!-- Single Content On Profile Ends -->
												</ul>
											</div>
											<!-- Single Profile Ends -->
											<!-- Social Media Ends -->
										</div>
									</div>
									<!-- Personal Info Ends -->
								</div>
								<!-- Content Ends -->
							</div>
							<!-- Inner Content Ends -->
						</div>
						<!-- Main Content (About) Ends -->
					</div>
					<!-- Left Wrapper Ends -->
					<!-- Right Wrapper Starts -->
					<div id="right-wrapper" class="col-lg-4">
						<!-- Large Profile Picture Starts -->
						<div class="lg-profile-picture"></div> 
						<!-- Large Profile Picture Ends -->
					</div>	
					<!-- Right Wrapper Ends -->
				</div>
			</div>
		</div>
		<!-- Wrapper Ends -->

		<!-- JS Files -->
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/imagesloaded.pkgd.min.js"></script>
		<script src="js/isotope.pkgd.min.js"></script>
		<script src="js/magnific-popup.min.js"></script>
		<script src="js/typeit.min.js"></script>
		<script src="js/custom.js"></script>
	</body>

<!-- Mirrored from riettsruff.com/projects/web-templates/metronal/preview/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 13 Apr 2019 11:38:31 GMT -->
</html>